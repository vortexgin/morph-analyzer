# Morphological Analyzer
Morphological analyzer is a simple PHP Library to analyze word sentence in Indonesian Language (Bahasa Indonesia). This tools is an implementation of the [Morphind Project](http://septinalarasati.com/work/morphind/) that created by [Septina Larasati](http://septinalarasati.com/).


## Cara Install
Morphological analyzer dapat diinstall dengan [Composer](https://getcomposer.org/). Tambahkan configurasi berikut di dalam file composer.json kamu :
``
...
"minimum-stability": "dev", 
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/vortexgin/morph-analyzer"
    }
],
"require": {
  "vortexgin/morph-analyzer": "dev-master"
},  
...
``
Jika Anda masih belum memahami bagaimana cara menggunakan Composer, silahkan baca [Getting Started with Composer](https://getcomposer.org/doc/00-intro.md).

Setelah selesai menginstall, daftarkan library ke dalam AppKernel.php
``
public function registerBundles()
{
    $bundles = array(
      ...
      new Vortexgin\MorphAnalyzerBundle\VortexginMorphAnalyzerBundle(),      
      ...
    );
    return $bundles;
  }

``  

## Cara Menggunakan
``
/* @var $morphManager \Vortexgin\MorphAnalyzerBundle\Manager\MorphManager */
$morphManager = $this->container->get('vortexgin.morph.manager');

$morph = $morphManager->analyze($word);
``
